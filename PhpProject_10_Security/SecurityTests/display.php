<!DOCTYPE html>
<html>

<head>
    <title>Display Test</title>
</head>

<body>
    <?php
    $string = "'Who, Me!?', she exclaimed, in a rather naïve way.";

    echo "Unfiltered String: $string<br>";
    echo "Filtered String: " . htmlentities($string) . "<br>";
    echo "Filtered String with Options: " . htmlentities($string, ENT_QUOTES, "UTF-8");
    ?>
</body>

</html>