<?php
session_start();
if (isset($_GET['logout'])) {
    session_destroy();
    die("You have been logged out");
}

if (isset($_SESSION['username'])) {
    $loggedIn = true;
} else if (isset($_GET['username'])) {
    $_SESSION['username'] = $_GET['username'];
    $_SESSION['password'] = $_GET['password'];
    $_SESSION['country'] = $_GET['country'];

    if ($_SESSION['username'] == "admin" && $_SESSION['password'] == "admin123") {
        $loggedIn = true;
    }
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title></title>
</head>

<body>
    <h1>Region Login</h1>
    <?php
    if (isset($loggedIn)) {
        echo "<h2>Welcome {$_SESSION['username']}, you have logged in to the "
            . htmlentities($_SESSION['country']) . " portal.</h2>";
    } else {
    ?>
        <p>Please provide your username and password and choose one of your assigned regions.</p>
        <form action="" method="GET" name="greetingForm">
            <p><label for="username">Username</label>
                <input type="text" name="username" required>
            </p>
            <p><label for="password">Password</label>
                <input type="password" name="password" required>
            </p>
            <p><label for="country">Country</label>
                <select name="country">
                    <option value="malta">Malta</option>
                    <option value="italy">Italy</option>
                </select>
            </p>
            <p><input type="submit" value="Login"></p>
        </form>
    <?php } ?>
    <a href="?logout">Logout</a>
</body>

</html>