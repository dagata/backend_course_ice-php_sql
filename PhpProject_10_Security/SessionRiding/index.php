<?php
session_start();
if (isset($_POST['username'])) {
    if ($_POST['username'] == "user" && $_POST['password'] == "user") {
        $_SESSION['access_level'] = "user";
    } else if ($_POST['username'] == "admin" && $_POST['password'] == "admin") {
        $_SESSION['access_level'] = "admin";
    }
}
?>

<html>

<head>
    <meta charset="UTF-8">
    <title></title>
</head>

<body>
    <?php
    if (isset($_SESSION['access_level'])) {
        echo "You are logged in as " . $_SESSION['access_level'];
    } else {
    ?>
        <form name="loginform" method="POST">
            <p>
                <label for="username">Username</label>
                <input type="text" name="username">
            </p>
            <p>
                <label for="password">Password</label>
                <input type="password" name="password">
            </p>
            <p>
                <label>&nbsp;</label>
                <input type="submit" value="Login">
            </p>
        </form>
    <?php } ?>
</body>

</html>