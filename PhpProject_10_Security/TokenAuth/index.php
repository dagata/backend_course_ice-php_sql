<?php
session_start();

//Generate the session token
if (empty($_SESSION['token'])) {
    $_SESSION['token'] = bin2hex(random_bytes(32));
}
$token = $_SESSION['token'];

//Generate the login form token
if (empty($_SESSION['formToken'])) {
    $_SESSION['formToken'] = bin2hex(random_bytes(32));
}
$formToken = hash_hmac('sha256', '/index.php', $_SESSION['formToken']);

if (isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] == 1) {
    echo "You are logged in as {$_SESSION['accessLevel']}";
    die();
}

if (isset($_POST['username'])) {
    //Check the tokens
    if (
        !empty($_POST['token']) && hash_equals($_SESSION['token'], $_POST['token'])
        && !empty($_POST['formToken']) && hash_equals($formToken, $_POST['formToken'])
    ) {
        if ($_POST['username'] == "admin" && $_POST['password'] == "admin") {
            $_SESSION['loggedIn'] = 1;
            $_SESSION['accessLevel'] = "Administrator";
            session_regenerate_id();
            echo "You are logged in as {$_SESSION['accessLevel']}";
        } else if ($_POST['username'] == "user" && $_POST['password'] == "user") {
            $_SESSION['loggedIn'] = 1;
            $_SESSION['accessLevel'] = "Standard";
            session_regenerate_id();
            echo "You are logged in as {$_SESSION['accessLevel']}";
        } else {
            echo "Invalid login";
        }
    } else {
        echo "WARNING: Missing token or token mismatch";
        die();
    }
}

if (!isset($_SESSION['loggedIn'])) {
?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>

    <body>
        <form name='loginForm' method="POST">
            <input type="text" name="username">
            <input type="password" name="password">
            <input type="hidden" name="token" value="<?= $token ?>">
            <input type="hidden" name="formToken" value="<?= $formToken ?>">
            <input type="submit">
        </form>
    </body>

    </html>
<?php
}
