<?php
require 'DBConnect.php';

class Chat {
    protected $db;
    
    public function __construct() {
        $this->db = DBConnect::getInstance();
    }
    
    public function getChats() {
        $st = $this->db->getHandler()->prepare("SELECT * FROM Chat ORDER BY id ASC");
        $st->execute();
        $result = $st->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }
    
    public function addChat($who, $message) {
        $st = $this->db->getHandler()->prepare("INSERT INTO Chat(who, message) VALUES (:who, :message)");
        $st->bindParam(':who', $who);
        $st->bindParam(':message', $message);
        $st->execute();
    }
}
