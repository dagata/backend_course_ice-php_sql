<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span> 
            </button>
            <a class="navbar-brand" href="#/home">Job Application System</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li id="menu-home" class="active"><a href="#/home">Home</a></li>
                <li id="menu-applicants"><a href="#/applicants">Applicants</a></li> 
                <li id="menu-apply"><a href="#/apply">Apply</a></li> 
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#/signup"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <li><a href="#/login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            </ul>
        </div>
    </div>
</nav>