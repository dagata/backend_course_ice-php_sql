<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="vendor/twbs/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="css/template.css" rel="stylesheet" type="text/css" />
    <title>Job Application System</title>
</head>

<body>
    <?php require 'includes/menu.php' ?>

    <div id="pageContainer" class="container" role="main">
        <?php require 'view/home_view.php' ?>
    </div>

    <script src="vendor/components/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="vendor/twbs/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="lib/director.min.js" type="text/javascript"></script>
    <script src="js/router.js" type="text/javascript"></script>
    <script src="lib/jquery.validate.min.js" type="text/javascript"></script>
    <script src="lib/additional-methods.min.js" type="text/javascript"></script>
</body>

</html>