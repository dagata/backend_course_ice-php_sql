$(function() {
    bindValidation();
});

var bindValidation = function() {
    $('#applyForm').validate({
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        submitHandler: function(form) {        
            theUrl = 'router.php?/rest/applicant/add';
            var params = $(form).serialize();
            $.ajax ({
                type: "POST",
                url: theUrl,
                data: params,
                processData: false,
                async: false,
                success: function(result) {
                    // console.log(result);
                    window.location = "#/applicants";
                },
                error: function(result) {
                    console.log(result);
                }
            });
        }
    });
};