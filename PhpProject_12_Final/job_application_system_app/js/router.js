var goHome = function() {
    $.get('router.php?/view/home', function(html) {
        $('#pageContainer').html(html);
        $('#myNavbar li').removeClass('active');
        $('#menu-home').addClass('active');
    });
};

var goApplicants = function() {
    $.get('router.php?/view/applicants', function(html) {
        $('#pageContainer').html(html);
        $('#myNavbar li').removeClass('active');
        $('#menu-applicants').addClass('active');
    });
};

var goApply = function() {
    $.get('router.php?/view/apply', function(html) {
        $('#pageContainer').html(html);
        $('#myNavbar li').removeClass('active');
        $('#menu-apply').addClass('active');
    });
};

var goEditApplicant = function(applicantId) {
    console.log('Edit applicant ' + applicantId);
};

var routes = {
    '/home': goHome,
    '/applicants': goApplicants,
    '/apply': goApply,
    '/applicants/:applicantId': goEditApplicant
};

var router = Router(routes);
router.init();