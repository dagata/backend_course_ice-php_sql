<?php
require 'vendor/autoload.php';

$q = str_replace('?', "", filter_input(INPUT_SERVER, 'QUERY_STRING', FILTER_SANITIZE_URL));

$query = explode('/', $q);
if (count($query) == 3) {
    $queryType = $query[1];
    $action = $query[2];
} else if (count($query) == 4) {
    $queryType = $query[1];
    $stub = $query[2];
    $action = $query[3];
}

if ($queryType == "view") {
    include "view/{$action}_view.php";
} else if ($queryType == "rest") {
    include "src/ws/$stub.php";
}
