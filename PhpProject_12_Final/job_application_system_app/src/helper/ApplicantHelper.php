<?php

namespace com\icemalta\jobapp\helper;

use com\icemalta\jobapp\model\{Entity as Entity, Applicant as Applicant};

/**
 * Description of Applicant
 */
class ApplicantHelper extends Helper implements TablePrinter
{
    public function add(Entity $applicant): int
    {
        if ($applicant instanceof Applicant) {
            $firstName = $applicant->getFirstName();
            $lastName = $applicant->getLastName();
            $email = $applicant->getEmail();

            $query = $this->db->prepare("INSERT INTO Applicant(firstName, lastName, email)" .
                " VALUES(:firstName, :lastName, :email)");
            $query->bindParam(':firstName', $firstName);
            $query->bindParam(':lastName', $lastName);
            $query->bindParam(':email', $email);
            $query->execute();
            return $this->db->lastInsertId();
        }
        return 0;
    }

    public function delete(int $id)
    {
        $query = $this->db->prepare("DELETE FROM Applicant WHERE id = :id");
        $query->bindParam(':id', $id);
        $query->execute();
    }

    public function get(int $id): Entity
    {
        $query = $this->db->prepare("SELECT * FROM Applicant WHERE id =:id");
        $query->bindParam(':id', $id);
        $query->execute();
        $result = $query->fetch(\PDO::FETCH_OBJ);
        $applicant = new Applicant(
            $result->firstName,
            $result->lastName,
            $result->email
        );
        return $applicant;
    }

    public function getAll(): array
    {
        $returnArr = [];

        $query = $this->db->prepare("SELECT * FROM Applicant");
        $query->execute();
        $result = $query->fetchAll(\PDO::FETCH_OBJ);

        foreach ($result as $record) {
            $applicant = new Applicant(
                $record->firstName,
                $record->lastName,
                $record->email
            );
            array_push($returnArr, $applicant);
        }
        return $returnArr;
    }

    public function update(int $id, Entity $applicant)
    {
        $this->delete($id);
        $this->add($applicant);
    }

    public function printTable()
    {
        $returnTable = <<<EOT
                <table class="table table-hover">
                    <thead>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                    </thead>
                    <tbody>
EOT;
        $applicants = $this->getAll();
        foreach ($applicants as $applicant) {
            $returnTable .= <<<EOT
                    <tr>
                        <td>{$applicant->getFirstName()}</td>
                        <td>{$applicant->getLastName()}</td>
                        <td>{$applicant->getEmail()}</td>
                    </tr>
EOT;
        }
        $returnTable .= '</tbody></table>';
        echo $returnTable;
    }
}
