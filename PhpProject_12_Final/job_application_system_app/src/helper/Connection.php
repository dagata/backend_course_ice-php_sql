<?php

namespace com\icemalta\jobapp\helper;

/**
 * Singleton to connect to the database.
 */
class Connection
{
    private static $_singleton;
    private $_connection;

    const DB_USERNAME = 'root';
    const DB_PASSWORD = '';
    const DB_HOST = 'localhost';
    const DB_NAME = 'JobApplicationSystem';

    private function __construct()
    {
        $this->_connection = new \PDO(
            'mysql:host=' . self::DB_HOST . ';dbname=' . self::DB_NAME,
            self::DB_USERNAME,
            self::DB_PASSWORD
        );
        $this->_connection->exec("SET CHARACTER SET utf8");
    }

    public static function getInstance(): Connection
    {
        if (is_null(self::$_singleton)) {
            self::$_singleton = new Connection();
        }
        return self::$_singleton;
    }

    public function getHandler()
    {
        return $this->_connection;
    }
}
