<?php

namespace com\icemalta\jobapp\helper;

use com\icemalta\jobapp\model\Entity as Entity;

/**
 * Description of Helper
 */
abstract class Helper
{
    protected $db;

    public function __construct()
    {
        $this->db = Connection::getInstance()->getHandler();
    }

    public abstract function add(Entity $entity): int;
    public abstract function delete(int $id);
    public abstract function update(int $id, Entity $entity);
    public abstract function get(int $id): Entity;
    public abstract function getAll(): array;
}
