<?php

namespace com\icemalta\jobapp\helper;

interface TablePrinter
{
    public function printTable();
}
