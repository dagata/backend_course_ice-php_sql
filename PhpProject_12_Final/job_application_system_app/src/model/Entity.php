<?php
namespace com\icemalta\jobapp\model;

/**
 * Generic parent class for any entity class
 */
class Entity {
    private $id;
    
    public function __construct() {
        $this->id = 0;
    }
    
    public function getId(): int {
        return $this->id;
    }
    
    public function setId(int $id) {
        if(isset($id) && $id != NULL) {
            $this->id = $id;
        } else {
            $this->id = 0;
        }
    }
}
