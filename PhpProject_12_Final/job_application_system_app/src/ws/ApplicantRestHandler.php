<?php
namespace com\icemalta\jobapp\ws;

use com\icemalta\jobapp\model\Applicant as Applicant;
use com\icemalta\jobapp\helper\ApplicantHelper as ApplicantHelper;
use com\icemalta\jobapp\ws\SimpleRest as SimpleRest;

/**
 * Description of ApplicantRestHandler
 */
class ApplicantRestHandler extends SimpleRest {
    public function addApplicant($firstName, $lastName, $email) {
        $helper = new ApplicantHelper();
        $helper->add(new Applicant($firstName, $lastName, $email));
    }
}
