<?php
use com\icemalta\jobapp\ws\ApplicantRestHandler as ApplicantRestHandler;

switch($action) {
    case "add":
        $handler = new ApplicantRestHandler();
        $firstName = filter_input(INPUT_POST, 'firstName', FILTER_SANITIZE_STRING);
        $lastName = filter_input(INPUT_POST, 'lastName', FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
        $handler->addApplicant($firstName, $lastName, $email);
        echo $firstName;
        break;
    default:
        // Do nothing
}
