<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>First PHP lesson!</title>
</head>

<body>
    <?php
    echo "Hello from PHP!<br>";
    echo "This is my first PHP lesson.";

    // Variables

    $dogName = 'Sparky'; //string
    $dogAge = 4; //int

    //Simple string - single quotes
    echo '<br>I have a dog. His name is ';
    echo $dogName;

    echo '<br>His age is ' . $dogAge;

    //Complex string - double quotes
    echo "<br>My dog name is $dogName.";

    bark();
    echo '<br>The total is ' . add(4, 9);


    //functions
    function bark()
    {
        echo '<br>Woof!';
    }

    function add($num1, $num2)
    {
        return $num1 + $num2;
    }

    //variables - always give a value - no value means is null
    $var; //null
    $var_1 = 0;
    $var_2 = "";
    $var_3 = true;
    $var_4 = false;
    $var_5 = 'Zoe';
    $var_6 = 28;
    $var_7 = 1;
    $var_8 = null;

    echo '<br>' . $var;
    echo '<br>' . $var_1;
    echo '<br>' . $var_2;
    echo '<br>' . $var_3;
    echo '<br>' . $var_4;
    echo '<br>' . $var_5;
    echo '<br>' . $var_6;
    echo '<br>' . $var_7;
    echo '<br>' . $var_8;
    echo '<br>';

    //var checking
    var_dump($var_1, $var_2, $var_3, $var_4, $var_5, $var_6, $var_7, $var_8);
    var_dump(is_bool($var_1));
    var_dump(isset($var));
    echo '<br>var: ' . isset($var);
    echo '<br>var_2: ' . isset($var_2);
    echo '<br>var_4: ' . isset($var_4);
    echo '<br>var_8: ' . isset($var_8);

    //Constants
    define('PI', 3.14);
    echo '<br>' . PI;
    echo '<br>' . defined('PI_2'); //false
    echo '<br>' . defined('PI'); //true
    const DATE_TODAY = '21jan'; //PHP 5.3 onwards
    echo '<br>' . DATE_TODAY;
    const DATE_TOMORROW = 22 . 'jan';
    echo '<br>' . DATE_TOMORROW; //PHP 5.6 onwards
    define('CONFIG_FILES', array(
        '/etc/httpd/httpd.conf',
        'C:\\php\\php.ini'
    ));
    echo '<br>';
    print_r(CONFIG_FILES);

    echo '<br>' . PHP_INT_MAX;


    //Type casting / type conversion
    $var_9 = 32.3467;
    echo '<br>Original: ' . $var_9;
    echo '<br>Cast to int: ' . (int)$var_9;
    echo '<br>Convert to int: ' . intval($var_9);

    //Variable variables - DO NOT USE!
    $item = 'UserName';
    $$item = 'Cod';
    echo "<br>item: $item";
    echo "<br>UserName: $UserName";

    //Printing out the values of an array
    $names = array('Dan', 'Mark', 'Tom', 'John');
    echo '<br>';
    print_r($names);
    var_dump($names);
    var_export($names); //PHP code

    //Modulus
    $firstNum = 11;
    $secondNum = 2;
    echo "<br>$firstNum / $secondNum = " . (int) ($firstNum / $secondNum);
    echo "<br>$firstNum / $secondNum = " . ($firstNum % $secondNum);

    //Increment and decrement operators
    $a = 0;
    $b = 0;
    $b = ++$a; //Prefix increment - associativity right to left
    echo '<br>a: ' . $a;
    echo "<br>b: $b";

    $b = $a++; //Postfix increment - associativity left to right
    echo "<br>a: $a";
    echo "<br>b: $b";

    $a--;
    echo "<br>a: $a";

    //Bitwise operators
    $ex1 = 5656;
    echo "<br> ~5656: " . (~5656); //Complement ~

    $ex2 = 56732;
    $ex3 = 23027;
    echo "<br>$ex2 & $ex3: " . ($ex2 & $ex3); //And &
    echo "<br>$ex2 | $ex3: " . ($ex2 | $ex3); //Or |
    echo "<br>$ex2 ^ $ex3: " . ($ex2 ^ $ex3); //XOr ^

    $ex4 = 90;
    echo "<br>left shift: " . ($ex4 << 2); //Left Shift <<
    echo "<br>right shift: " . ($ex4 >> 2); //Right Shift <<

    //Assignment with operation
    //Compound assignment operator
    $ex5 = 5; //Assignment operator =
    $ex5 += 6; //Compound operators $ex5 = $ex5 + 6;
    echo "<br>ex5 = $ex5";

    //Assignment by Value vs Reference
    $v1 = 7;
    $v2 = $v1;
    $v2 = 8;
    echo "<br>$v1 ,$v2"; //By value = default

    $v3 = 7;
    $v4 = &$v3;
    $v4 = 8;
    echo "<br>$v3 ,$v4"; //By reference

    //Relational operators
    $x = 10;
    $y = 7;
    echo "<br> x > y " . ($x > $y);
    echo "<br> x < y " . ($x < $y);
    $x = 'ABC'; //in ASCII 'C'=67
    $y = 'ABD'; //in ASCII 'D'=68
    echo "<br> x < y " . ($x < $y); //Strings converted to binary


    //Comparison / Equality Operators
    $p = 10; //int
    $q = 10.0; //float
    echo "<br> p == q: " . ($p == $q);
    echo "<br> p === q: " . ($p === $q);
    echo "<br> p != q: " . ($p != $q);
    echo "<br> p !== q: " . ($p !== $q);

    //logical operators
    $a = true;
    echo '<br>' . !$a; //Operator !

    $a = 18;
    $b = false;
    echo '<br>' . $a > 16 || $b; //Operator || or OR

    $a = 18;
    $b = false;
    echo '<br>' . $a > 16 && $b; //Operator & or AND

    $a = 18;
    $b = true;
    echo '<br>' .  $a > 16 xor $b; //Operator XOR

    //The Backtick Operator
    $files = `dir*`;
    print_r($files);

    //IF Statements
    $personAge = 23;

    if ($personAge >= 25) {
        echo "<br>You are allowed in the casino.";
    } else {
        echo "<br>You're too young to gamble!";
    }

    // An if statement using multiple conditions
    $pizzaChoice = 5;

    if ($pizzaChoice === 1) {
        echo "<br>Margherita";
    } elseif ($pizzaChoice === 2) {
        echo "<br>Funghi";
    } elseif ($pizzaChoice === 3) {
        echo "<br>Napoletana";
    } elseif ($pizzaChoice === 4) {
        echo "<br>Marinara";
    } else {
        echo "<br>Invalid pizza choice!";
    }

    // Ternary operator 
    // (operand)?(operand):(operand)
    $userAge = 20;
    $message = $userAge >= 18 ? "You can drive" : "You cannot drive";
    echo "<br>$message";

    // Loops
    $hoursAllowedFishing = 6;
    $hour = 7;

    // while loop (pre-tested)
    echo "<br>";
    while ($hour <= $hoursAllowedFishing) {
        echo "Fishing for hour $hour <br>";
        $hour++;
    }

    // for loop (pre-tested)
    $hour = 1;
    for ($hour = 7; $hour <= $hoursAllowedFishing; $hour++) {
        echo "<br>Fishing for hour $hour";
    }

    // do-while loop (post-tested)
    $hour = 7;
    echo "<br>";
    do {
        echo "<br>Fishing for hour $hour";
        $hour++;
    } while ($hour <= $hoursAllowedFishing);

    // for-each loop
    $list = array("Bob", "Kate", "Sue", "Frank", "Martha");

    foreach ($list as $name) {
        echo "<br>Hello, $name";
    }

    $names = array("Bob", "Kate", "Sue");

    foreach ($names as $key => $value) {
        echo '<br>' . $key . ':' . $value;
    }

    // The break and Continue statements
    for (;;) {
        $randNum = rand(1, 10);
        if ($randNum === 2) {
            break;
        }
        echo "<br>randNum = $randNum";
    }

    echo "<br>------------<br>";
    for ($i = 0; $i < 20; $i++) {
        $randNum1 = rand(1, 10);
        if ($randNum1 % 2 == 0) {
            continue;
        }
        echo "<br>randNum1 = $randNum1";
    }

    $hoursAllowedFishing = 6;
    $fishingHour = 1;
    $lunchBreak = 4;
    for (;;) {
        if ($fishingHour === $lunchBreak) {
            echo "<br>Taking lunch break.";
            $fishingHour++;
            continue;
        }
        echo "<br>Fishing for hour $fishingHour.";
        $fishingHour++;
        if ($fishingHour > $hoursAllowedFishing) {
            break;
        }
    }


    //Switch statement
    $pastaChoice = 2;
    switch ($pastaChoice) {
        case 1:
            echo "<br>Carbonara";
            break;
        case 2:
            echo "<br>Caccio e pepe";
            break;
        case 3:
            echo "<br>Puttanesca";
            break;
        default:
            echo "<br>Invalid choice";
    }



    ?>
</body>

</html>