<?php

/*FUNCTIONS - a called function can return a value, 
a reference (some area in memory), or another fct. */

//Passing values by value (default)
function square($number)
{
    return $number * $number;
}

$a = 4;
$a = square($a);
echo "<br>By value: $a";

//Passing values by reference
function square2(&$number)
{
    $number *= $number;
}

$b = 4;
square2($b);
echo "<br>By reference: $b";

//Default values / optional arguments
function power($number, $power = 2)
{
    return pow($number, $power);
}

$c = 4;
echo "<br>" . power($c);
echo "<br>" . power($c, 4);

//Variable number of arguments using func_get_args()
function sum()
{
    $total = 0;
    foreach (func_get_args() as $val) {
        $total += $val;
    }
    return $total;
}

echo '<br>Sum: ' . sum(34, 2, 19);

//PHP5.6+ doing the above using variadics (syntactic sugaring)
function sum2(...$numbers)
{
    $total = 0;
    foreach ($numbers as $val) {
        $total += $val;
    }
    return $total;
}

echo '<br>Sum: ' . sum(34, 2, 19);



//Returning a reference
$providers = array("Vmware", "Parallels", "Oracle", "Hyper-V");

function &getProvider($itemNumber)
{
    global $providers;
    return $providers[$itemNumber];
}
echo '<br>';
print_r($providers);
$providerName = &getProvider(1);
$providerName = "KVM";
echo '<br>';
print_r($providers);



//Returning a reference without using global/above more readable
function getProvider_1($itemNumber, &$providers)
{
    return $providers[$itemNumber];
}
$providerName = getProvider_1(1, $providers);
$providerName = "GCN";
echo '<br>';
print_r($providers);



//Variable Functions - used when not sure of function existence/output
function sayHello()
{
    echo "<br>Hello there!";
}

$functionName = 'sayHello';
if (function_exists($functionName)) {
    $functionName();
}



//Anonymous (Lambda) functions - in some cases can save time by comparison to named functions

function turnOnEngine()
{
    echo '<br>Engine is turning on...';
}

echo '<br>Starting driving process...';
turnOnEngine(); //if will delay will follow 'Driving' which is not possible!
echo '<br>Setting gear to 1';
echo '<br>Driving!';



//Anonymous functions - resolving above issue with callBack/variable functions
function turnOnEngine1($callBack)
{
    echo '<br>Engine is turning on...';
    $callBack();
}

function drive1()
{
    echo '<br>Driving!';
}

echo '<br>Starting driving process...';
turnOnEngine1("drive1"); // with cb function we are sure that Driving comes only after engineOn
echo '<br>Setting gear to 1';



//Anonymous functions - by avoiding to create another function drive1 and put it as arg. we obtain an anonymous fct. or throw-away fct.
function turnOnEngine2($callBack)
{
    echo '<br>Engine is turning on...';
    $callBack();
}

echo '<br>Starting driving process...';
turnOnEngine2(function () {
    echo '<br>Driving!';
});
echo '<br>Setting gear to 1';



//Anonymous functions - using multiple anonymous fct.
function turnOnEngine3($callBack)
{
    echo '<br>Engine is turning on...';
    $callBack();
}

echo '<br>Starting driving process...';
$reverse = true; //if False will 'Driving!' after engineOn
turnOnEngine3($reverse ?
    function () {
        echo '<br>Reversing!';
    } : function () {
        echo '<br>Driving!';
    });
echo '<br>Setting gear to 1';



//Closures = a function that returns a function or fct. generator
function multiplyBy10($number)
{
    return ($number * 10);
}

function multiplyBy25($number)
{
    return ($number * 25);
}

echo '<br>' . multiplyBy10(75);
echo '<br>' . multiplyBy25(75);

//This is the Closure:
$multiplyByN = function ($N) {
    return function ($number) use ($N) {
        return ($number * $N);
    };
};

$myFct = $multiplyByN(40);
echo '<br>' . $myFct(75);

//or

echo '<br>' . $multiplyByN(40)(75);
/*Without 'use' return an error due to non-existing $N or
    represents a nested fct;
    Inner fct doesn't have access to variables from outside;
    Closures allows to use var from outer fct inside an inner fct 
    using "use" keyword;
    Can work also with $N "global" but it's untidy;*/

/*or without assign the variable $multiplyByN but then
we couldn't pass fct."test" to a page/url */
function test($N)
{
    return function ($number) use ($N) {
        return ($number * $N);
    };
};

echo '<br>' . test(40)(75);

/*In PHP7.4 it was introduced a new more compact way of 
creating closures using arrow fct.*/

$multiplyByN = fn ($N) => fn ($number) => ($number * $N);



//Class and Aray type hinting - in front of the argument

//Function hint
function sendEmail(array $recipientList)
{
    //Implementation here
}

/*Class hint - The constructor of the Car class will only
                    accept an instance of the Driver class
                     as a parameter for its constructor. */
class Car
{
    protected $driver;
    public function __construct(Driver $driver)
    {
        $this->driver = $driver;
    }
}
class Driver
{
}
$driver1 = new Driver();
$car1 = new Car($driver1);



//Scope - Variables

$a = 3;
function tryMe1()
{
    $a = 0;
    $a += 2;
    echo '<br>' . $a;
}
tryMe1(); //2 - function result without interference from global $a = 3
echo '<br>' . $a; //3 - global var a=3

function tryMe2()
{
    global $a;
    $a += 2;
}
tryMe2();
echo "<br>$a<br>"; //5

/*  - The two statements below produce identical results.
    - Both create a variable in the function’s scope that
     is a reference to the same value as the variable $a
    in the global scope.*/
global $a;
$a = &$GLOBALS['a'];



//Static Variables - not destroyed after function termination

function tryMe()
{
    static $a = 0; //This runs only once
    $a++;
    echo $a . '<br>';
}
tryMe();
tryMe();
tryMe();



/*Recursion - a function that calls itself 
                Are used in traversing of folders/directories or
                string generation. 
            When to use? - code written using loops can also be
                written using recursive techniques. Recursion is
                neat when traversing a file system containing files and
                subfolders.
                Use of recursion is ideal when traversing a structure hierarchy of
                indeterminate depth.
                Recursive functions are however less efficient and slower than iterative
                techniques.*/

function bottleSong($number)
{
    if ($number == 1) {
        $string = "1 bottle of beer on the wall, 1 bottle of beer.<br>";
        $string .= "Take one down and pass it around, no more bottles of beer on the wall.<br>";
        $string .= "<br>No more bottles of beer on the wall, no more bottles of beer.<br>";
        $string .= "Go to the store and buy some more, 99 bottles of beer on the wall.";
        return $string;
    } else {
        $string = "$number bottles of beer on the wall, $number bottles of beer.<br>";
        $phrase = --$number == 1 ? "bottle" : "bottles";
        $string .= "Take on down and pass it around, $number $phrase of beer on the wall.<br>";
        $string .= "<br>";
        return $string . bottleSong($number); //Head Recursive
    }
}
$song = bottleSong(5);
echo '<br>' . $song;

/*Direct and Indirect Recursion
    Direct recursion - bottlesong above;
    Indirect recursion - when a function is invoked as part of function calls
        Example: f() calls g(), g() calls h(), h() calls f(). */

/*Head and Tail Recursion - example on factorial fct below
    Tail Recursion - the fct doesn't have to do anything after
        fct returns, other than return its value.
    Head Recursion - the result of each fct must be added to the 
        result of all previous ones. */

function factorial1($number, $accumulator = 1)
{
    if ($number == 0) {
        return $accumulator;
    }
    return factorial1($number - 1, $number * $accumulator);
}
$result = factorial1(5);
echo '<br>' . $result; //120 - tail recursive


function factorial2($number)
{
    if ($number == 0) {
        return 1;
    }
    return $number * factorial2($number - 1);
}
$result = factorial2(5);
echo '<br>' . $result; //120 - head recursive
