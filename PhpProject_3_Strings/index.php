<?php

//Simple and Complex Strings

$today = "22nd July";

echo '<br>Single quotes: $today'; //Simple string, also known as literal string
echo "<br>Double quotes: $today"; //Complex string, supports var interpolation

$name = "Lucy";
echo "<br>Today is {$name}'s birthday!";

$months = array('Jan', 'Feb', 'Mar');
echo "<br>My birthday is in the month: {$months[1]}";
echo "<br>$months[1]'s weather is rainy.";


//Variable Interpolation

$notPrinted = 'Some text...';
$aVariable = '$notPrinted';
echo '<br>' . "$aVariable"; //Extension to 'Some text...' is never performed, only one step above


//HereDoc Strings - complex string that can occupy multiple lines

$place = "Dreamlandia";
$story = <<<STORY
        There was an old man from $place,
        who dreamt he was eating his shoe.
STORY; //Instead of 'STORY' can be any word/letter
echo '<br>' . $story;

//NowDoc Strings - simple multiline strings

$place = "Dreamlandia";
$story = <<<'STORY'
        There was an old man from $place,
        who dreamt he was eating his shoe.
STORY;
echo '<br>' . $story;


//Printf - use format specifiers to insert strings in strings and/or to format it.

echo '<br>';
printf("I had a %s and %s icecream.", "chocolate", "vanilla");

$itemPrice = 31.478;
printf("<br>The item cost %.2f euro.", $itemPrice);

$studentNumbers = array(23, 478, 12, 987);
foreach ($studentNumbers as $num) {
    printf("<br>Student %04d", $num);
}


//String Length

$aString = "A string of characters";
$stringLength = strlen($aString);
echo '<br>' . $stringLength;


/*Working with HTML entities (< > &) = (&lt; &gt; &amp;) PHP output in source codefile
        Old browsers issue or when extracting from db's. */

$string = htmlentities("< > &");
echo '<pre>';
echo $string;
echo '</pre>';

echo "<br> This is a > test"; // > will appear due to newer browers

$input = "Joe <strong>Borg</strong>";
echo "<br>" . $input;
echo "<br>" . strip_tags($input);


// Strings comparison

$str1 = 5;
$str2 = '5';
if ($str1 === $str2) {
    echo '<br>They are the same';
} else {
    echo '<br>They are not the same';
}


$item1 = 'A';
$item2 = 'B';
$result = strcmp($item1, $item2);
echo "<br>Result: $result"; //0 if same(in ASCII A=65, B=66), -1 if 1st <<, 1 if 1st >>


//Substring search

$text = "I like bacon";
echo '<br>';
echo substr($text, 7);
echo '<br>';
echo substr($text, 7, 3);
echo '<br>';
echo substr($text, -5);


// Count occurrences

$text2 = "Hello my baby, hello my darling.";
echo '<br>';
echo substr_count(strtolower($text2), 'hello');


// Exploding a string into tokens

$text3 = "Dan, Vassallo, 99998878, dan@yahoo.com";
$data = explode(', ', $text3);
echo '<br>';
print_r($data);

$text4 = implode(', ', $data);
echo "<br>$text4";


// Tokenizing strings using strtok

$text5 = "78, Rosie, Flat 1, Wilga Street, Paceville, PCV001, Malta";
$token = strtok($text5, ',');  // Iterable
while ($token !== false) {
    echo "<br>$token";
    $token = strtok(',');
}


// Tokenizing strings but specify a data type

$text6 = "192.168.23.1:8080";
$items = sscanf(str_replace(':', ' ', $text6), "%s %d");
var_dump($items);


// Search

$text7 = "dan@yahoo.com";
$pos = strpos($text7, '@');
echo "<br>$pos";
if (strpos($text7, '@') !== false) {
    echo "<br>$text7 is a valid email";
} else {
    echo "<br>$text7 is not a  valid email";
}


// Parsing a URL

$url = "https://dan:pass@yahoo.com:8080/material/mswdtest?id=34#profile";
$bits = parse_url($url);
echo '<br>';
print_r($bits);


// Basic Regex

$master_text = "she sells sea shells on the seashore";
echo '<br>';
echo preg_match('/the/', $master_text);
echo '<br>';
echo preg_match('/she/', $master_text);
echo '<br>';
echo preg_match_all('/she/', $master_text);


// special characters

echo '<br>';
echo preg_match('/^she/', $master_text); // starts with
echo '<br>';
echo preg_match('/shore$/', $master_text); // ends with