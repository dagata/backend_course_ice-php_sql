<?php

$books = array(
    "The Hitchhiker's Guide to the Galaxy", "War and Peace", "The Art of War", "PHP For Beginners",
    "A History of Germany", "The Alt Right Movement", "Catcher in the Rye", "The Future of Bitcoin",
    "101 Yogurt Recipes"
);
$showResults = (isset($_POST['operation']) && $_POST['operation'] != "none") ? true : false;

if ($showResults) {
    $operation = $_POST['operation'];
    $operand = $_POST['operand'];
    if ($operation == "startsWith") {
        $regex = "/^$operand/";
    } else if ($operation == "endsWith") {
        $regex = "/{$operand}$/";
    } else if ($operation == "contains") {
        $regex = "/$operand/";
    } else {
        $regex = $operand;
    }
    $filteredArray = preg_grep($regex, $books);
}

?>


<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Regex Tester</title>
    <style>
        label {
            display: inline-block;
            min-width: 100px;
            text-align: right;
        }
    </style>
</head>

<body>
    <h1>Regex Tester</h1>

    <?php if ($showResults) { ?>
        <h2>Filter Results (<?php echo $regex; ?>)</h2>
        <ul>
            <?php
            foreach ($filteredArray as $matchingBook) {
                echo "<li>$matchingBook</li>";
            }
            ?>
        </ul>
    <?php } ?>

    <h2>Available Books</h2>

    <ul>
        <?php
        foreach ($books as $book) {
            echo "<li>$book</li>";
        }
        ?>
    </ul>

    <form name="filterForm" method="POST" action="index.php">
        <fieldset>
            <legend>Use this form to filter the books</legend>
            <select name="operation">
                <option selected value="none">Do Nothing</option>
                <option value="startsWith">Starts With</option>
                <option value="endsWith">Ends With</option>
                <option value="contains">Contains</option>
                <option value="custom">Custom</option>
            </select>
            <input type="text" name="operand" id="operand">
            <input type="submit" name="filter" id="filter" value="Filter">
        </fieldset>
    </form>
</body>

</html>