<?php
// Session start
session_start();

// Array to hold the students
if (isset($_SESSION['studentList'])) {
    // Data in session found, so set the array to that data
    $studentList = $_SESSION['studentList'];
} else {
    // We didn't find anything in the session, so create a new empty array
    $studentList = array();
}

$action = null; // Prepare a placeholder for current action
// Check if we received an 'action' GET parameter
if (isset($_GET['action'])) {
    // We did! So save the value in the placeholder
    $action = $_GET['action'];
}

// Decide what to do, based on the value of $action
switch ($action) {
    case "add":
        addStudent();
        break;
    case "delete":
        deleteStudent();
        break;
    case "sort":
        sortStudents();
        break;
}

function addStudent()
{
    global $studentList;
    $studentList[] = array(
        "name" => $_POST['name'],
        "surname" => $_POST['surname'],
        "grade" => $_POST['grade']
    );
    $_SESSION['studentList'] = $studentList;
    sortStudents();
}

function deleteStudent()
{
    global $studentList;
    $studId = $_GET['studId'];
    unset($studentList[$studId]);
    $_SESSION['studentList'] = $studentList;
    sortStudents();
}

function sortStudents()
{
    global $studentList;
    $sortBy = "id"; // Default sort is by student id

    if (isset($_GET['sortBy'])) {
        // User has specified a sort
        $sortBy = $_GET['sortBy'];
        $_SESSION['sortBy'] = $sortBy;
    } else if (isset($_SESSION['sortBy'])) {
        // Use previous sort
        $sortBy = $_SESSION['sortBy'];
    }

    if ($sortBy == 'id') {
        // Sort by the keys of the array
        ksort($studentList);
    } else {
        // Sort by one of the fields of the student
        uasort($studentList, function ($a, $b) use ($sortBy) {
            if ($a[$sortBy] == $b[$sortBy]) return 0;
            return ($a[$sortBy] > $b[$sortBy]) ? 1 : -1;
        });
    }
}

?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Student Management System</title>
</head>

<body>
    <h1>Add Student</h1>
    <form name="addStudent" method="POST" action="index.php?action=add">
        <fieldset>
            <legend>Fill in this form to add a student</legend>

            <label for="name">Name</label>
            <input type="text" name="name">
            <br>
            <label for="surname">Surname</label>
            <input type="text" name="surname">
            <br>
            <label for="grade">Grade</label>
            <input type="text" name="grade">
            <br>
            <input type="submit" value="Add">
        </fieldset>
    </form>

    <h1>Student List</h1>
    <table border="1">
        <thead>
            <tr>
                <th><a href="index.php?action=sort&sortBy=id">ID</a></th>
                <th><a href="index.php?action=sort&sortBy=name">Name</a></th>
                <th><a href="index.php?action=sort&sortBy=surname">Surname</a></th>
                <th><a href="index.php?action=sort&sortBy=grade">Grade</a></th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($studentList as $id => $student) {
                echo "<tr>";
                echo "<td>$id</td>";
                echo "<td>{$student['name']}</td>";
                echo "<td>{$student['surname']}</td>";
                echo "<td>{$student['grade']}</td>";
                echo "<td><a href=\"index.php?action=delete&studId=$id\">Delete</a></td>";
                echo "</tr>";
            }
            ?>
        </tbody>
    </table>

</body>

</html>