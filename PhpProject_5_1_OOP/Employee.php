<?php

class Employee
{
    private $name;
    private $surname;
    private $hourlyRate;
    private $monthHours;
    private $overtimeAllowed;

    const TAX_RATE = 15;
    const CURRENCY = '€';

    public function __construct($name, $surname, $hourlyRate, $overtimeAllowed)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->hourlyRate = $hourlyRate;
        $this->overtimeAllowed = $overtimeAllowed;
        $this->monthHours = 160;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getMonthHours()
    {
        return $this->monthHours;
    }

    public function setMonthHours($monthHours)
    {
        $this->monthHours = $monthHours;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    public function getHourlyRate()
    {
        return $this->hourlyRate;
    }

    public function setHourlyRate($hourlyRate)
    {
        $this->hourlyRate = $hourlyRate;
    }

    public function isOvertimeAllowed()
    {
        return $this->overtimeAllowed;
    }

    public function setOvertimeAllowed($overtimeAllowed)
    {
        $this->overtimeAllowed = $overtimeAllowed;
    }

    public function getPayslip($hoursWorked)
    {
        // Calculate standard hours
        $standardHours = 0;
        if ($hoursWorked >= $this->monthHours) {
            $standardHours = $this->monthHours;
        } else {
            $standardHours = $hoursWorked;
        }

        // Calculate overtime hours
        $overtimeHours = 0;
        if ($this->overtimeAllowed && ($hoursWorked > $this->monthHours)) {
            $overtimeHours = $hoursWorked - $this->monthHours;
        }

        $standardGross = $standardHours * $this->hourlyRate;
        $standardTax = $standardGross * (self::TAX_RATE / 100);
        $standardNet = $standardGross - $standardTax;

        $overtimeGross = $overtimeHours * ($this->hourlyRate * 1.5);
        $overtimeTax = $overtimeGross * (self::TAX_RATE / 100);
        $overtimeNet = $overtimeGross - $overtimeTax;

        $totalGross = $standardGross + $overtimeGross;
        $totalTax = $standardTax + $overtimeTax;
        $totalNet = $standardNet + $overtimeNet;

        $payslip = new stdClass();
        $payslip->standardHours = $standardHours;
        $payslip->overtimeHours = $overtimeHours;
        $payslip->standardGross = $standardGross;
        $payslip->standardTax = $standardTax;
        $payslip->standardNet = $standardNet;
        $payslip->overtimeGross = $overtimeGross;
        $payslip->overtimeTax = $overtimeTax;
        $payslip->overtimeNet = $overtimeNet;
        $payslip->totalGross = $totalGross;
        $payslip->totalNet = $totalNet;
        $payslip->totalTax = $totalTax;

        return $payslip;
    }
}
