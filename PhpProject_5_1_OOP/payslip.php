<?php
require 'Employee.php';
session_start();

$employeeList = $_SESSION['employeeList'];
$empId = $_POST['empId'];
$hoursWorked = $_POST['hoursWorked'];
$month = $_POST['slipMonth'];
$year = $_POST['slipYear'];

$employee = $employeeList[$empId];
$payslip = $employee->getPayslip($hoursWorked);
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>
        <?php
        echo "Payslip for {$employee->getName()} {$employee->getSurname()} for $month $year";
        ?>
    </title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
    <div class="wrapper">
        <div class="bar">
            Payslip
        </div>
        <div class="info">
            <span class="info">Employee:</span>
            <?php echo "{$employee->getName()} {$employee->getSurname()} <br>"; ?>
            <span class="info">Basis:</span>
            <?php echo "$month $year <br>"; ?>
            <span class="info">Tax Bracket:</span>
            <?php echo Employee::TAX_RATE . "%"; ?>
        </div>
        <div class="bar">
            Breakdown
        </div>
        <div class="info">
            <table>
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Qty</th>
                        <th>Gross</th>
                        <th>Tax</th>
                        <th>Net</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Standard Hours</td>
                        <td><?= $payslip->standardHours; ?></td>
                        <td><?php echo Employee::CURRENCY . number_format($payslip->standardGross); ?></td>
                        <td><?php echo Employee::CURRENCY . number_format($payslip->standardTax); ?></td>
                        <td><?php echo Employee::CURRENCY . number_format($payslip->standardNet); ?></td>
                    </tr>
                    <tr>
                        <td>Overtime Hours</td>
                        <td><?= $payslip->overtimeHours; ?></td>
                        <td><?php echo Employee::CURRENCY . number_format($payslip->overtimeGross); ?></td>
                        <td><?php echo Employee::CURRENCY . number_format($payslip->overtimeTax); ?></td>
                        <td><?php echo Employee::CURRENCY . number_format($payslip->overtimeNet); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="bar">
            Totals
        </div>
        <div class="info">
            <span class="info">Total Gross:</span><?php echo Employee::CURRENCY . number_format($payslip->totalGross); ?><br>
            <span class="info">Total Tax:</span><?php echo Employee::CURRENCY . number_format($payslip->totalTax); ?><br>
            <span class="info">Total Net:</span><?php echo Employee::CURRENCY . number_format($payslip->totalNet); ?><br>
        </div>
    </div>
</body>

</html>