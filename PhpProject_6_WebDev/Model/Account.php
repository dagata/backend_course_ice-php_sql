<?php

namespace UserAccounts\Model;

use \PDO;

require 'DBConnect.php';

class Account {
    private $id;
    private $firstName;
    private $lastName;
    private $username;
    private $password;

    public function __construct($firstName, $lastName, $username, $password, $id=null) {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->username = $username;
        $this->password = $password;
    }

    public static function add($account) {

        $hashed = password_hash($account->getPassword(), PASSWORD_DEFAULT);

        try {
            $db = new DBConnect();
            $conn = $db->getConnection();
        } catch (Exception $ex) {
            throw $ex;
        }

        $stat = $conn->prepare(
            "INSERT INTO Account(username, password, firstName, lastName) " .
            "VALUES (:username, :password, :firstName, :lastName)"
        );
        $stat->bindValue(':username', $account->getUsername());
        $stat->bindValue(':password', $hashed);
        $stat->bindValue(':firstName', $account->getFirstName());
        $stat->bindValue(':lastName', $account->getLastName());

        try {
            $stat->execute();
        } catch (Exception $ex) {
            throw $ex;
        }

        return $conn->lastInsertId();
    }

    public static function getAccount($username, $password) {
        try {
            $db = new DBConnect();
            $conn = $db->getConnection();
        } catch (Exception $ex) {
            throw $ex;
        }

        $stat = $conn->prepare("SELECT * FROM Account WHERE username = :username");
        $stat->bindParam(':username', $username);

        try {
            $stat->execute();
        } catch (Exception $ex) {
            throw $ex;
        }

        $result = $stat->fetch(PDO::FETCH_OBJ);

        if ($result && password_verify($password, $result->password)) {
            return new Account(
                $result->firstName,
                $result->lastName,
                $result->username,
                $result->password,
                $result->id
            );
        } else {
            return null;
        }
    } 

    public static function isUsernameAvailable($username) {
        try {
            $db = new DBConnect();
            $conn = $db->getConnection();
        } catch (Exception $ex) {
            throw $ex;
        }

        $stat = $conn->prepare("SELECT id FROM Account WHERE username = :username");
        $stat->bindParam(":username", $username);

        try {
            $stat->execute();
        } catch (Exception $ex) {
            throw $ex;
        }

        $result = $stat->fetch(PDO::FETCH_OBJ);

        return !($result && isset($result->id));
    }

    public function getId() {
        return $this->id;
    }

    public function getFirstName() {
        return $this->firstName;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }
    
    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }
    
    public function getUsername() {
        return $this->username;
    }
    
    public function setUsername($username) {
        $this->username = $username;
    }
    
    public function getPassword() {
        return $this->password;
    }
    
    public function setPassword($password) {
        $this->password = $password;
    }
}