<?php

namespace UserAccounts\Model;

use \PDO;

class DBConnect
{
    private static $DB_USERNAME = "root";
    private static $DB_PASSWORD = "";
    private static $DB_HOST = "localhost";
    private static $DB_SCHEMA = "UserAccounts";
    private static $DB_TYPE = "mysql";

    public static function getConnection()
    {
        try {
            $conn = new PDO(
                self::$DB_TYPE . ':dbname=' . self::$DB_SCHEMA . ';host=' . self::$DB_HOST,
                self::$DB_USERNAME,
                self::$DB_PASSWORD
            );
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $ex) {
            throw $ex;
        }

        return $conn;
    }
}
