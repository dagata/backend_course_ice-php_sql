<?php
session_start();

include_once 'Model/Model.php';

class Controller {
    public $model;


    public function __construct() { $this->model = new Model(); }

    public function invoke() {
        if (isset($_GET['book'])) {
            // Load the given book
            $book = $this->model->getBook($_GET['book']);
            include 'View/viewbook.php';
        } else if (isset($_GET['admin'])) {
            // User wants to access admin screen
            if (isset($_SESSION['adminUser'])) {
                // User is already logged in
                include 'View/admin.php';
            } else if (isset($_POST['username']) && isset($_POST['password'])) {
                // User has just logged in
                $validLogin = $this->model->checkLogin($_POST['username'], $_POST['password']);
                if ($validLogin) {
                    $_SESSION['adminUser'] = $_POST['username'];
                    include 'View/admin.php';
                } else {
                    $loginError = true;
                    include 'View/login.php';
                }
            } else {
                // Show login screen
                include 'View/login.php';
            }
        } else if (isset($_GET['addBook'])) {
            // User wants to add a book
            $this->model->addBook($_POST['title'], $_POST['author'], $_POST['description']);
            $books = $this->model->getBookList();
            include 'View/booklist.php';
        } else if (isset($_GET['logout'])) {
            // Log the user out
            session_destroy();
            $books = $this->model->getBookList();
            include 'View/booklist.php';
        } else {
            $books = $this->model->getBookList();
            include 'View/booklist.php';
        }
    }
}