<?php
include_once('Book.php');
include_once('DB.php');

class Model
{

    private $conn;

    public function __construct()
    {
        $this->conn = DB::getInstance();
    }

    public function getBookList()
    {
        $stat = $this->conn->getHandler()->prepare("SELECT * FROM Book");
        $stat->execute();
        $bookList = $stat->fetchAll(PDO::FETCH_OBJ);
        return $bookList;
    }

    public function getBook($id)
    {
        $stat = $this->conn->getHandler()->prepare("SELECT * FROM book WHERE id = :id");
        $stat->bindParam(':id', $id);
        $stat->execute();
        $book = $stat->fetch(PDO::FETCH_OBJ);
        return $book;
    }

    public function checkLogin($username, $password)
    {
        $stat = $this->conn->getHandler()->prepare(
            "SELECT * FROM admin WHERE username = :username"
        );
        $stat->bindParam(':username', $username);
        $stat->execute();
        $result = $stat->fetch(PDO::FETCH_OBJ);

        if ($result && password_verify($password, $result->password)) {
            return true;
        }
        return false;

        /* 
            Admin account:
            User: "admin"
            Pass: "Trump2020"
            */
    }

    public function addBook($title, $author, $description)
    {
        $stat = $this->conn->getHandler()->prepare(
            "INSERT INTO book(title, author, description) VALUES (:title, :author, :description)"
        );
        $stat->bindParam(':title', $title);
        $stat->bindParam(':author', $author);
        $stat->bindParam(':description', $description);
        $stat->execute();
    }
}
