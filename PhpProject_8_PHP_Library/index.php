<?php
$library = new SimpleXMLElement('library.xml', NULL, true);

$book = $library->addChild('book');
$book->addAttribute("isbn", "0812550706");
$book->addChild("title", "Ender's Game");
$book->addChild("author", "Orson Scott Card");
$book->addChild("publisher", "Tor Science Fiction");

foreach ($library->book as $book) {
    printf("%s<br>", $book['isbn']);
    printf("%s<br>", $book->title);
    printf("%s<br>", $book->author);
    printf("%s<br>", $book->publisher);
    echo "---<br>";
}

// XPath example - Get the title of each book
$results = $library->xpath('/library/book/title');
echo "<br>--XPATH--<br>";
foreach ($results as $title) {
    printf("%s<br>", $title);
}
echo "---<br>";

// Get the title of the first book
$result = $library->book[0]->xpath('title');
echo $result[0];
