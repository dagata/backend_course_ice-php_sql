<?php
require_once 'ArtWork.php';

function handleClient($method, $args = '')
{
    switch ($method) {
        case 'POST':
            $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $year = filter_input(INPUT_POST, 'year', FILTER_SANITIZE_NUMBER_INT);
            $artist = filter_input(INPUT_POST, 'artist', FILTER_SANITIZE_STRING);
            $medium = filter_input(INPUT_POST, 'medium', FILTER_SANITIZE_STRING);
            new ArtWork($title, $year, $artist, $medium);
            var_dump(http_response_code(201));
            break;
        case 'DELETE':
            ArtWork::delete($args[0]);
            var_dump(http_response_code(200)); // optional
            break;
        case 'GET':
            if ($args == '') {
                echo json_encode(ArtWork::getAll());
            } else {
                $art = ArtWork::get($args[0]);
                if ($art) {
                    echo json_encode($art);
                } else {
                    var_dump(http_response_code(404));
                }
            }
            break;
        default:
            header('HTTP/1.1 405 Method Not Allowed');
            header('Allowed: GET, POST, DELETE');
            break;
    }
}

$method = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];
$pathList = explode('/', $uri);
$resource = $pathList[3];

if ($resource == 'artwork' && !isset($pathList[4])) {
    handleClient($method);
} else if ($resource == 'artwork' && isset($pathList[4])) {
    handleClient($method, array($pathList[4]));
}

// http:// 0
// localhost/ 1
// artwork/ 2
// 1&sort=name&limit=10 3

// localhost/ArtDB/artwork/4 --> localhost/ArtDB/index.php/artwo