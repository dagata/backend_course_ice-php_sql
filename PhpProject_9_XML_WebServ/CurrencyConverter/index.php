<?php

try {
    $opts = array(
        'http' => array(
            'user_agent' => 'PHPSoapClient'
        )
    );
    $context = stream_context_create($opts);

    $wsdlUrl = 'http://currencyconverter.kowabunga.net/converter.asmx?wsdl';
    $soapClientOptions = array(
        'stream_context' => $context,
        'cache_wsdl' => WSDL_CACHE_NONE
    );

    $client = new SoapClient($wsdlUrl, $soapClientOptions);
    var_dump($client->__getFunctions());
} catch (Exception $ex) {
    printf("<pre>%s</pre>", $ex->getMessage());
}

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $currencyFrom = filter_input(INPUT_POST, 'currencyFrom', FILTER_SANITIZE_STRING);
    $currencyTo = filter_input(INPUT_POST, 'currencyTo', FILTER_SANITIZE_STRING);
    $amount = filter_input(INPUT_POST, 'amount', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

    $params = array(
        'CurrencyFrom' => $currencyFrom,
        'CurrencyTo' => $currencyTo,
        'RateDate' => (new DateTime())->format('Y-m-d'),
        'Amount' => $amount
    );

    $conversionResult = $client->GetConversionAmount($params)->GetConversionAmountResult;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Currency Conversion SOAP Client</title>
</head>

<body>
    <?php
    if (isset($conversionResult)) {
        printf("<h2>%.2f%s = %.2f%s</h2>", $amount, $currencyFrom, $conversionResult, $currencyTo);
    }
    ?>
    <form name="conversionForm" action="" method="POST">
        <p>
            <label for="currencyFrom">From</label>
            <select name="currencyFrom" id="currencyFrom">
                <?php
                foreach ($currencies as $currency) {
                    echo "<option value='$currency'>$currency</option>";
                }
                ?>
            </select>
        </p>
        <p>
            <label for="currencyTo">To</label>
            <select name="currencyTo" id="currencyTo">
                <?php
                foreach ($currencies as $currency) {
                    echo "<option value='$currency'>$currency</option>";
                }
                ?>
            </select>
        </p>
        <p>
            <label for="amount">Amount</label>
            <input type="number" name="amount" id="amount" required>
        </p>
        <p>
            <label>&nbsp;</label>
            <input type="submit" value="Convert">
        </p>
    </form>
</body>

</html>